-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 18-Jan-2018 às 21:45
-- Versão do servidor: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `etecreplay`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aluno`
--

CREATE TABLE `aluno` (
  `rm` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `dta_nasc` varchar(10) NOT NULL,
  `sexo` char(1) NOT NULL,
  `email` varchar(40) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `foto` varchar(100) NOT NULL DEFAULT 'semfoto.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso`
--

CREATE TABLE `curso` (
  `cod_curso` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `modulos` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina`
--

CREATE TABLE `disciplina` (
  `sigla` char(6) NOT NULL,
  `ementa` text,
  `cod_curso` int(11) NOT NULL,
  `nome_disc` varchar(40) NOT NULL,
  `cod_professor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina_turma`
--

CREATE TABLE `disciplina_turma` (
  `sigla` char(6) NOT NULL,
  `modulo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `matricula_aluno`
--

CREATE TABLE `matricula_aluno` (
  `rm` int(11) NOT NULL,
  `ano` int(4) NOT NULL,
  `semestre` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mensagem`
--

CREATE TABLE `mensagem` (
  `id_mensagem` int(11) NOT NULL,
  `cod_prof` int(11) NOT NULL,
  `rm` int(11) NOT NULL,
  `nome_aluno` varchar(40) NOT NULL,
  `email_aluno` varchar(50) NOT NULL,
  `mensagem` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `professor`
--

CREATE TABLE `professor` (
  `cod_prof` int(11) NOT NULL,
  `nome_prof` varchar(50) NOT NULL,
  `dt_nasc_prof` char(10) NOT NULL,
  `email_prof` varchar(50) NOT NULL,
  `tel_prof` int(13) NOT NULL,
  `sigla` char(6) NOT NULL,
  `senha` varchar(32) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `turma`
--

CREATE TABLE `turma` (
  `modulo` int(1) NOT NULL,
  `ano` int(4) NOT NULL,
  `semestre` int(1) NOT NULL,
  `rm` int(11) NOT NULL,
  `cod_curso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `video`
--

CREATE TABLE `video` (
  `id_video` int(11) NOT NULL,
  `cod_curso` int(11) NOT NULL,
  `sigla` char(6) NOT NULL,
  `cod_prof` int(11) NOT NULL,
  `nome_video` varchar(200) NOT NULL,
  `descricao` text,
  `tags` varchar(200) DEFAULT NULL,
  `link_video` varchar(50) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aluno`
--
ALTER TABLE `aluno`
  ADD PRIMARY KEY (`rm`);

--
-- Indexes for table `curso`
--
ALTER TABLE `curso`
  ADD PRIMARY KEY (`cod_curso`);

--
-- Indexes for table `disciplina`
--
ALTER TABLE `disciplina`
  ADD PRIMARY KEY (`sigla`,`nome_disc`,`cod_curso`),
  ADD KEY `fk_cod_curso_disc` (`cod_curso`);

--
-- Indexes for table `disciplina_turma`
--
ALTER TABLE `disciplina_turma`
  ADD PRIMARY KEY (`sigla`,`modulo`),
  ADD KEY `fk_modulo_disc_turma` (`modulo`);

--
-- Indexes for table `matricula_aluno`
--
ALTER TABLE `matricula_aluno`
  ADD PRIMARY KEY (`rm`,`ano`,`semestre`);

--
-- Indexes for table `mensagem`
--
ALTER TABLE `mensagem`
  ADD PRIMARY KEY (`id_mensagem`),
  ADD KEY `cod_prof` (`cod_prof`),
  ADD KEY `rm` (`rm`);

--
-- Indexes for table `professor`
--
ALTER TABLE `professor`
  ADD PRIMARY KEY (`cod_prof`),
  ADD UNIQUE KEY `cod_prof` (`cod_prof`),
  ADD KEY `fk_sigla_disc_prof` (`sigla`);

--
-- Indexes for table `turma`
--
ALTER TABLE `turma`
  ADD PRIMARY KEY (`modulo`,`ano`,`semestre`),
  ADD KEY `fk_rm_turma_matri` (`rm`),
  ADD KEY `fk_cod_curso_turma_curso` (`cod_curso`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id_video`),
  ADD KEY `cod_prof` (`cod_prof`),
  ADD KEY `cod_prof_2` (`cod_prof`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mensagem`
--
ALTER TABLE `mensagem`
  MODIFY `id_mensagem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `professor`
--
ALTER TABLE `professor`
  MODIFY `cod_prof` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id_video` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `disciplina`
--
ALTER TABLE `disciplina`
  ADD CONSTRAINT `disciplina_ibfk_1` FOREIGN KEY (`cod_curso`) REFERENCES `curso` (`cod_curso`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `disciplina_turma`
--
ALTER TABLE `disciplina_turma`
  ADD CONSTRAINT `disciplina_turma_ibfk_1` FOREIGN KEY (`sigla`) REFERENCES `disciplina` (`sigla`) ON DELETE CASCADE,
  ADD CONSTRAINT `disciplina_turma_ibfk_2` FOREIGN KEY (`modulo`) REFERENCES `turma` (`modulo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `matricula_aluno`
--
ALTER TABLE `matricula_aluno`
  ADD CONSTRAINT `fk_rm_matricula_aluno` FOREIGN KEY (`rm`) REFERENCES `aluno` (`rm`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `mensagem`
--
ALTER TABLE `mensagem`
  ADD CONSTRAINT `mensagem_ibfk_1` FOREIGN KEY (`cod_prof`) REFERENCES `professor` (`cod_prof`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mensagem_ibfk_2` FOREIGN KEY (`rm`) REFERENCES `aluno` (`rm`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `professor`
--
ALTER TABLE `professor`
  ADD CONSTRAINT `fk_sigla_disc_prof` FOREIGN KEY (`sigla`) REFERENCES `disciplina_turma` (`sigla`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `turma`
--
ALTER TABLE `turma`
  ADD CONSTRAINT `fk_cod_curso_turma_curso` FOREIGN KEY (`cod_curso`) REFERENCES `curso` (`cod_curso`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_rm_turma_matri` FOREIGN KEY (`rm`) REFERENCES `matricula_aluno` (`rm`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `video_ibfk_1` FOREIGN KEY (`cod_prof`) REFERENCES `professor` (`cod_prof`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
