<?php
session_start();
require_once("servidor.php");
if (!empty($_SESSION['mensagem'])) {
	echo "<div class='btn-success'>";
	echo "<center>".$_SESSION['mensagem']."</center> 
	<a href='index.php'><span class='glyphicon glyphicon-remove' style='position:absolute;margin-left:85%; color:red;'></span></a>";
	unset($_SESSION['mensagem']);
	echo "</div>";
}
if (!empty($_SESSION['erro'])) {
	echo "<div class='btn-danger'>";
	echo "<center>".$_SESSION['erro']."</center> 
	<a href='index.php'><span class='glyphicon glyphicon-remove' style='position:absolute;margin-left:85%; color:red;'></span></a>";
	unset($_SESSION['erro']);
	echo "</div>";
}
session_destroy();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link rel="shortcut icon" type="image/png" href="favicon.ico"/>
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="css/estilo.css">
		<title>EtecReplay - Login</title>
	</head>
	<body style="overflow-x: hidden;">
<div class="tudo">
		<div class="row">
			<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 arealogin">
				<div class="btn btn-block logo">
					<br><br>
					<center>
					<img src="Etec.png" width="40%">
					<img src="iconereplay.png" class="etec">
				</center>
				</div>
				<!--AreaLogin-->
				<div class="row">
					<div class="col-xs-1 col-sm-2 col-md-4 col-lg-4"></div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
						<fieldset class="login">
							<form action="checagem.php" method="post">
								<label>
									<span><font color="black">RM (Aluno) / E-Mail (Professor)</font></span><br>
									<input type="text" name="login" placeholder=" Digite seu email ou RM" class="loginesenha"/>
								</label><br>
								<label>
									<span class="senha"><font color="black">Senha</font></span><br>
									<input type="password" name="senha" placeholder=" Digite sua senha" class="loginesenha"/>
								</label><br>
								<select name="tipo" class="selecao">
									<option class="opcao" value="aluno">Aluno</option>
									<option class="opcao" value="professor">Professor</option>
								</select>
								
									<button type="submit" id="botao" name="enviar">Entrar</button>
								
							</form>
						</fieldset>
					</div>
					<div class="col-xs-5 col-sm-4 col-md-2 col-lg-2"></div>
				</div>
				<!-- fim AreaLogin-->
				<!--BR-->
				<div style="margin-top:350px;">
								
				
				</div>
				<!--BR-->
			</div>
			<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
			
			</div>
		</div>
		<footer style="margin-left:0px;
		height: 100%;
		background-color: #DCDCDC;
		border-style: solid;
  	  	border-width: 2px 0px 0px 0px">
  	  	<br><br>
		<a href="admin/login/index.php"><img src="logo_CPS.png" width="20%" style="float: right; margin-right: 40px;"></a>
		<img src="fundo.png" width="20%">
		</footer>
		</body>
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.js"></script>
	</html>