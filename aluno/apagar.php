<?php
session_start();
if (isset($_SESSION['tipo'])) {
	if ($_SESSION['tipo']=="aluno") {
	}else{
		$_SESSION['mensagem']="Você não tem permissão para ver esta página";
		header("location:../");
		exit;
	}
}
require_once("servidor.php");
if (isset($_POST['apagar'])) {
	if (!empty($_POST['id_mensagem'])) {
		$id_mensagem=$_POST['id_mensagem'];
		$comando="DELETE FROM mensagem WHERE id_mensagem = '$id_mensagem'";
		$enviar=mysqli_query($conn, $comando);
		if ($enviar) {
			$_SESSION['mensagem']="Mensagem excluida";
			header("location:index.php");
			exit;
		}else{
			$_SESSION['mensagem']="Erro ao excluir mensagem";
			header("location:index.php");
			exit;
		}
	}
}else{
	header("location:index.php");
	exit;
}

?>