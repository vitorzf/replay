 <?php
session_start();
if (isset($_SESSION['tipo'])) {
	if ($_SESSION['tipo']=="aluno") {
	}else{
		$_SESSION['erro']="Você não tem permissão para ver esta página";
		header("location:../");
		exit;
	}
}
require_once("servidor.php");
if (isset($_POST['vervideo'])) {
	if (!empty($_POST['id_video'])) {
		$curso = $_SESSION['curso'];
		$video=$_POST['id_video'];
		$comando="SELECT * FROM video WHERE id_video = '$video'";
		$enviar=mysqli_query($conn, $comando);
		$videos=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
		foreach ($videos as $video) {
			$nomevideo=$video['nome_video'];
			$link=$video['link_video'];
			$descricao=$video['descricao'];
			$data=$video['data'];
			$id_professor=$video['cod_prof'];
			$sigla=$video['sigla'];
			$id_video_atual=$video['id_video'];
		}
		$comando="SELECT * FROM professor WHERE cod_prof = '$id_professor'";
		$enviar=mysqli_query($conn, $comando);
		$professor= mysqli_fetch_all($enviar, MYSQLI_ASSOC);
		foreach ($professor as $professor){
			$professor=$professor['nome_prof'];
		}
		$comando="SELECT * FROM turma WHERE cod_curso = '$curso'";
		$enviar=mysqli_query($conn, $comando);
		$turma= mysqli_fetch_all($enviar, MYSQLI_ASSOC);
		foreach ($turma as $modulo_atual){
			$modulo=$modulo_atual['modulo'];
		}
	}
}else{
	$_SESSION['mensagem']="Nenhum vídeo selecionado";
	header("location:./");
	exit;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<!--<span class="glyphicon glyphicon-headphones" aria-hidden="true">-->
		<title>Etec Replay - <?=$nomevideo?></title>
		<link rel="shortcut icon" type="image/png" href="../favicon.ico"/>
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="estilo.css">
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.js"></script>
		
	</head>
	<body style="overflow-x: hidden;">
		<nav class="navbar navbar-inverse">
			<div class="row">
				<div  class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-1">
					<a href="index.php"><img class="img-responsive" src="etecRlogo.png" style="margin: 14px 0px 0px 25px"></a>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-10">
					<ul class="nav navbar-nav">
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="index.php">Página Inicial</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="disciplina.php">Disciplinas</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="procurar.php">Procurar </a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="sobre.php">Sobre Nós </a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block" style="position: absolute; margin-left: 30%;"><a href="sair.php">Sair</a></li>
					</ul>
					<div class="dropdown visible-xs-block visible-xs-inline visible-xs-inline-block visible-sm-block visible-sm-inline visible-sm-inline-block" style="margin-top:10px; margin-left: 70%;" >
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width: 50px; height: 50px; background-color: black;">
						<span class="glyphicon glyphicon-align-justify"></span></button>
						<ul class="dropdown-menu">
							<li><a href="index.php">Pagina Inicial</a></li>
							<li><a href="disciplina.php">Disciplinas</a></li>
							<li><a href="procurar.php">Disciplinas</a></li>
							<li><a href="sobre.php">Sobre Nós</a></li>
							<li class="divider"></li>
							<li><a href="sair.php">Sair</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<style type="text/css" media="screen">
			.borda{
			 border:1px solid black;
			}
		</style>
		<div class="container">
			<div class="well borda" style="margin-top: -4px;">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-0 col-sm-0 col-md-1 col-lg-1"></div>
							<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
								<div style="margin-top: -10px;">
								<?php
								$comando="SELECT foto FROM professor WHERE cod_prof = '$id_professor'";
								$enviar=mysqli_query($conn, $comando);
								$resul=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
								if ($resul) {
									foreach ($resul as $foto) {
										$foto=$foto['foto'];
									}
								}
								$data=explode("-", $data);
								$data= $data[2]."/".$data[1]."/".$data[0];
								$novadesc=wordwrap($descricao, 69, "<br>",true);
								?>
								<strong><?=$professor?></strong> <img src="../professor/fotos/<?=$foto?>" class="img-circle" width="55px" height="55px">
							</div>
								<center><strong><font size="5" color="black"><?=strtoupper($nomevideo)?></font></strong></center><br>
								<iframe width="100%" height="462px" src="https://www.youtube-nocookie.com/embed/<?=$link?>?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen class="borda"></iframe>
							<br>
							<br>
							<div class="thumbnail borda">
							<font size="3px"><?=$novadesc?></font><br><br><br>
							<strong>Aula :</strong> <?=$sigla?>
							<strong>Módulo :</strong> <?=$modulo?><br>
							<strong>Data :</strong> <?=$data?><br>
							<form action="mensagem.php" method="post">
							<input type="hidden" name="id_professor" value="<?=$id_professor?>"><br>
							<button type="submit" name="mensagem" class="btn btn-info cartas">
							<img class="carta" src="carta.png"><font style="margin-left: 36px;">Enviar Mensagem</font></button>
							</form>

							<br>
							</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
								<br><br>
						<font size="3px" style="margin-left: 10px"><strong>Outros vídeos</strong></font><br><br>
							<?php
							$comando="SELECT DISTINCT  * FROM video WHERE sigla = '$sigla' AND id_video != '$id_video_atual' ORDER BY rand() LIMIT 1";
							$enviar=mysqli_query($conn, $comando);
							$resultado=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
							if ($resultado) {
								foreach ($resultado as $vedeo) {
									$thumb=$vedeo['link_video'];
									$nome=$vedeo['nome_video'];
									$id_video_sugerido=$vedeo['id_video'];
									$sigla=$vedeo['sigla'];
									?>
							<form action="video.php" method="post" style="float:left;margin-left: -10px;margin-right: 10px;">
							<input type="hidden" name="sigla" value="<?=$sigla?>">
							<input type="hidden" name="id_video" value="<?=$id_video_sugerido?>">
							<button class="videos" type="submit" name="vervideo"><img src="http://i1.ytimg.com/vi/<?=$thumb?>/mqdefault.jpg" width="140px" class="borda"><br>
								<center><?=$nome?></center>
							</button>
							<br><br>
						</form>
						
							<?php
								}
							}
							if (empty($id_video_sugerido)) {
								$id_video_sugerido=0;
							}
	$curso=$_SESSION['curso'];
			$comando="SELECT * FROM disciplina WHERE cod_curso = '$curso'";
			$enviar=mysqli_query($conn, $comando);
			$cursos=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
			foreach ($cursos as $curso) {
				$sigla=$curso['sigla'];
				$comando="SELECT * FROM video WHERE sigla = '$sigla' AND id_video != '$id_video_atual' AND id_video != '$id_video_sugerido' ORDER BY rand() LIMIT 3";
			$enviar=mysqli_query($conn, $comando);
			$videos=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
			if ($videos) {
			foreach($videos as $video){
			$id_aula=$video['id_video'];
			$nomevideo=$video['nome_video'];
			$data=$video['data'];
			$link=$video['link_video'];
			$sigla=$video['sigla'];
		?>
	<form action="video.php" method="post" style="float:left;margin-left: -10px;margin-right: 10px;" >
	<input type="hidden" name="id_video" value="<?=$id_aula?>">
	<button class="videos" type="submit" name="vervideo"><img src="http://i1.ytimg.com/vi/<?=$link?>/mqdefault.jpg" width="140px" class="borda"><br>
	<font size="2px"><center><?=$nomevideo?></center></font>
	<br>
	</button>
	</form>
	<?php
		}
	}
							
						}
							?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
		<br>
		<div class="row">
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/Etec_logo.png" class="img-responsive" width="80%"></center>
			</div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/cpslogo.png" class="img-responsive chao" width="90%"></center>
			</div>
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
		</div>
		<br><br><br><br>
	</div>
</body>
</html>