<?php
session_start();
include_once("servidor.php");
if (isset($_SESSION['tipo'])) {
	if ($_SESSION['tipo']=="aluno") {
	}else{
		$_SESSION['erro']="Você não tem permissão para ver esta página";
		header("location:../");
		exit;
	}
}
if (!empty($_SESSION['mensagem'])) {
	echo "<div class='btn-success'>";
	echo "<center>".$_SESSION['mensagem']."</center> 
	<a href='index.php'><span class='glyphicon glyphicon-remove' style='position:absolute;margin-left:85%; color:red;'></span></a>";
	unset($_SESSION['mensagem']);
	echo "</div>";
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<!--<span class="glyphicon glyphicon-headphones" aria-hidden="true">-->
		<title> EtecReplay - Procurar</title>
		<link rel="shortcut icon" type="image/png" href="../favicon.ico"/>
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="estilo.css">
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.js"></script>
		
	</head>
	<body style="overflow-x: hidden;">
		<nav class="navbar navbar-inverse">
			<div class="row">
				<div  class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-1">
					<a href="index.php"><img class="img-responsive" src="etecRlogo.png" style="margin: 14px 0px 0px 25px"></a>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-10">
					<ul class="nav navbar-nav">
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="index.php">Página Inicial</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="disciplina.php">Disciplinas</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="procurar.php">Procurar </a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="sobre.php">Sobre Nós </a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block" style="position: absolute; margin-left: 30%;"><a href="sair.php">Sair</a></li>
					</ul>
					<div class="dropdown visible-xs-block visible-xs-inline visible-xs-inline-block visible-sm-block visible-sm-inline visible-sm-inline-block" style="margin-top:10px; margin-left: 70%;" >
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width: 50px; height: 50px; background-color: black;">
						<span class="glyphicon glyphicon-align-justify"></span></button>
						<ul class="dropdown-menu">
							<li><a href="index.php">Pagina Inicial</a></li>
							<li><a href="disciplina.php">Disciplinas</a></li>
							<li><a href="procurar.php">Procurar</a></li>
							<li><a href="sobre.php">Sobre Nós</a></li>
							<li class="divider"></li>
							<li><a href="sair.php">Sair</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="well borda">
				<div class="row"><!--divide pelas colunas da tela-->
				<div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<center>
			<form action="procurar.php" method="post" accept-charset="utf-8">
				<input type="text" name="pesquisa" placeholder=" Pesquisar" required style="width: 300px;">
				<button type="submit" style="position:absolute;border:none;background-color: transparent;color: red;
				margin-left: -30px;margin-top: 2px;"><font size="4px"><span class="glyphicon glyphicon-search"></span></font></button>
			</form>
		</center>

		<?php
		if (isset($_POST['escolhaaula'])) {
			$pesquisa=$_POST['sigla'];

			//pesquisa sigla completa

			$comando="SELECT * FROM disciplina WHERE sigla = '$pesquisa'";
		$enviar=mysqli_query($conn, $comando);
		$recebe=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
		if ($recebe) {
			foreach ($recebe as $disciplina) {
				$nomedisc=$disciplina['nome_disc'];
			}
		}
			//fim pesquisa
			$comando="SELECT * FROM video WHERE sigla = '$pesquisa'";
		$enviar=mysqli_query($conn, $comando);
		$recebe=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
		if ($recebe) {
			echo "<br><center><font size='4px'>Videos em <strong>$nomedisc</strong></font></center><br><br>";
			foreach ($recebe as $video) {
				$id_video=$video['id_video'];
				$link=$video['link_video'];
				$nomevideo=$video['nome_video'];
				$data=$video['data'];
				$data=explode("-", $data);
				$data= $data[2]."/".$data[1]."/".$data[0];
				?>
				<form action="video.php" method="post" style="float:left">
	<input type="hidden" name="id_video" value="<?=$id_video?>">
	<button class="videos" type="submit" name="vervideo"><br><img src="http://i1.ytimg.com/vi/<?=$link?>/mqdefault.jpg" width="200px" style="border: 1px solid black"><br>
	<center><font size="2%"><?=$nomevideo?> <br><center>(<?=$data?>)</center></font></center><br>
	</button>

	</form>

	<?php
}
}else{
		echo "<br><br><center><font size='4px'>Nenhum vídeo encontrado em <strong>$nomedisc</strong></font><br><br><br></center>";
	}


		}
		if(!empty($_POST['pesquisa'])){
			$pesquisa=$_POST['pesquisa'];
		$comando="SELECT * FROM video WHERE nome_video LIKE '%$pesquisa%' OR tags LIKE '%$pesquisa%' OR sigla LIKE '%$pesquisa%'";
		$enviar=mysqli_query($conn, $comando);
		$recebe=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
		if ($recebe) {
			echo "<center><font size='4px'>Resultados para <strong>$pesquisa</strong></font></center><br><br>";
			foreach ($recebe as $video) {
				$id_video=$video['id_video'];
				$link=$video['link_video'];
				$nomevideo=$video['nome_video'];
				$data=$video['data'];
				$data=explode("-", $data);
				$data= $data[2]."/".$data[1]."/".$data[0];
				?>
				<form action="video.php" method="post" style="float:left">
	<input type="hidden" name="id_video" value="<?=$id_video?>">
	<button class="videos" type="submit" name="vervideo"><br><img src="http://i1.ytimg.com/vi/<?=$link?>/mqdefault.jpg" width="200px" style="border: 1px solid black"><br>
	<center><font size="2%"><?=$nomevideo?> <br><center>(<?=$data?>)</center></font></center><br>
	</button>

	</form>
	<?php
			}
		}else{
			echo "<br><br><center><font size='4px'>Nenhum resultado na pesquisa por <strong>".$pesquisa."</strong><br><br> tente palavras chave ou siglas de disciplinas, exemplos: DS1, Bootstrap, CSS, HTML, ETC</font></center><br><br>";
		}
	}elseif(empty($pesquisa)){
		$curso=$_SESSION['curso'];
			echo "<br><center><font size='4px'><strong>Todos os vídeos em ".$_SESSION['nomecurso']."</strong></font></center><br><br>";
		//	$comando="SELECT * FROM disciplina WHERE cod_curso = '$curso'";
		//	$enviar=mysqli_query($conn, $comando);
		//	$cursos=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
		//	foreach ($cursos as $curso) {
		//		$sigla=$curso['sigla'];
			$comando="SELECT * FROM video WHERE cod_curso = '$curso' ORDER BY data DESC";
			$enviar=mysqli_query($conn, $comando);
			$videos=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
			if ($videos) {
			foreach($videos as $video){
			$id_video=$video['id_video'];
			$nomevideo=$video['nome_video'];
			$data=$video['data'];
			$link=$video['link_video'];
			$aula=$video['sigla'];
			$data=explode("-", $data);
			$data= $data[2]."/".$data[1];
		?>
	<form action="video.php" method="post" style="float:left">
	<input type="hidden" name="id_video" value="<?=$id_video?>">
	<button class="videos" type="submit" name="vervideo"><br><img src="http://i1.ytimg.com/vi/<?=$link?>/mqdefault.jpg" width="200px" style="border: 1px solid black"><br>
	<center><font size="2%"><?=$nomevideo?> <br><center>(<?=$data?>)</center></font></center><br>
	</button>

	</form>
	<?php
		}
	}else{
		echo "<center><font size='4px'><strong>Nenhum vídeo encontrado</strong></font></center>";
	}
}

	
		?>
		<!--Fim pegar videos-->
		<!--Exibir os videos-->
		</center>

	</div>
</div>
</div>
</div>
	<br><br><br><br><br>
	<div style="margin-top:200px;"></div>
	<div class="footer">
		<br>
		<div class="row">
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/Etec_logo.png" class="img-responsive" width="80%"></center>
			</div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/cpslogo.png" class="img-responsive chao" width="90%"></center>
			</div>
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
		</div>
		<br><br><br><br>
	</div>
</body>
</html>