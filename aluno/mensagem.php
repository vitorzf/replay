<?php
session_start();
if (isset($_SESSION['tipo'])) {
	if ($_SESSION['tipo']=="aluno") {
	}else{
		$_SESSION['mensagem']="Você não tem permissão para ver esta página";
		header("location:../");
		exit;
	}
}
include_once("servidor.php");
if (isset($_POST['mensagem'])) {
	if (!empty($_POST['id_professor'])) {
		$id_professor=$_POST['id_professor'];
	}
}else{
	header("location:index.php");
	exit;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<!--<span class="glyphicon glyphicon-headphones" aria-hidden="true">-->
		<title>EtecReplay - Enviar Mensagem</title>
		<link rel="shortcut icon" type="image/png" href="../favicon.ico"/>
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="estilo.css">
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.js"></script>
		
	</head>
	<body style="overflow-x: hidden;">
		<nav class="navbar navbar-inverse">
			<div class="row">
				<div  class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-1">
					<a href="index.php"><img class="img-responsive" src="etecRlogo.png" style="margin: 14px 0px 0px 25px"></a>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-10">
					<ul class="nav navbar-nav">
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="index.php">Página Inicial</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="disciplina.php">Disciplinas</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="procurar.php">Procurar </a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="sobre.php">Sobre Nós </a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block" style="position: absolute; margin-left: 30%;"><a href="sair.php">Sair</a></li>
					</ul>
					<div class="dropdown visible-xs-block visible-xs-inline visible-xs-inline-block visible-sm-block visible-sm-inline visible-sm-inline-block" style="margin-top:10px; margin-left: 70%;" >
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width: 50px; height: 50px; background-color: black;">
						<span class="glyphicon glyphicon-align-justify"></span></button>
						<ul class="dropdown-menu">
							<li><a href="index.php">Pagina Inicial</a></li>
							<li><a href="disciplina.php">Disciplinas</a></li>
							<li><a href="procurar.php">Procurar</a></li>
							<li><a href="sobre.php">Sobre Nós</a></li>
							<li class="divider"></li>
							<li><a href="sair.php">Sair</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="well borda">
				<div class="row"><!--divide pelas colunas da tela-->
				<div  class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				<div  class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
				<?php
				$comando="SELECT * FROM professor WHERE cod_prof = '$id_professor'";
				$enviar=mysqli_query($conn, $comando);
				$professor=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
				foreach ($professor as $professor) {
					$nome_professor=$professor['nome_prof'];
				?>
				<form action="enviarmensagem.php" method="post" accept-charset="utf-8">
					<input type="hidden" name="id_professor" value="<?=$id_professor?>">
					<font size="3px">A mensagem será enviada para : <strong><?=$nome_professor?></strong>
					</font><br><br>
					<textarea name="mensagem" style="width: 100%; height: 200px;" placeholder="Escreva aqui a sua mensagem"></textarea><br><br>
					<center><button type="submit" name="enviar" class="btn btn-info cartas" style="height: 40px;">
							<img class="carta" src="carta.png"><font style="margin-left: 36px;">Enviar Mensagem</font></button></center>
				</form>
				<?php
			}
				?>
				</div>
				<div  class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
			</div>
		</div>
	</div>
	<div class="footer">
		<br>
		<div class="row">
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/Etec_logo.png" class="img-responsive" width="80%"></center>
			</div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/cpslogo.png" class="img-responsive chao" width="90%"></center>
			</div>
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
		</div>
		<br><br><br><br>
	</div>
</body>
</html>