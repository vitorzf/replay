<?php
session_start();
include_once("servidor.php");

	if ($_SESSION['tipo']=="aluno") {
	}else{
		$_SESSION['erro']="Você não tem permissão para ver esta página";
		header("location:../");
		exit;
	}

if (!empty($_SESSION['mensagem'])) {
	echo "<div class='btn-success'>";
	echo "<center>".$_SESSION['mensagem']."</center> 
	<a href='index.php'><span class='glyphicon glyphicon-remove' style='position:absolute;margin-left:85%; color:red;'></span></a>";
	unset($_SESSION['mensagem']);
	echo "</div>";
}
if (!empty($_SESSION['erro'])) {
	echo "<div class='btn-danger'>";
	echo "<center>".$_SESSION['erro']."</center> 
	<a href='index.php'><span class='glyphicon glyphicon-remove' style='position:absolute;margin-left:85%; color:red;'></span></a>";
	unset($_SESSION['erro']);
	echo "</div>";
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<!--<span class="glyphicon glyphicon-headphones" aria-hidden="true">-->
		<title> EtecReplay - Aluno</title>
		<link rel="shortcut icon" type="image/png" href="../favicon.ico"/>
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="estilo.css">
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.js"></script>
		
	</head>
	<body style="overflow-x: hidden;">
		<nav class="navbar navbar-inverse">
			<div class="row">
				<div  class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-1">
					<a href="index.php"><img class="img-responsive" src="etecRlogo.png" style="margin: 14px 0px 0px 25px"></a>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-10">
					<ul class="nav navbar-nav">
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="index.php">Página Inicial</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="disciplina.php">Disciplinas</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="procurar.php">Procurar </a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="sobre.php">Sobre Nós </a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block" style="position: absolute; margin-left: 30%;"><a href="sair.php">Sair</a></li>
					</ul>
					<div class="dropdown visible-xs-block visible-xs-inline visible-xs-inline-block visible-sm-block visible-sm-inline visible-sm-inline-block" style="margin-top:10px; margin-left: 70%;" >
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width: 50px; height: 50px; background-color: black;">
						<span class="glyphicon glyphicon-align-justify"></span></button>
						<ul class="dropdown-menu">
							<li><a href="index.php">Pagina Inicial</a></li>
							<li><a href="disciplina.php">Disciplinas</a></li>
							<li><a href="procurar.php">Procurar</a></li>
							<li><a href="sobre.php">Sobre Nós</a></li>
							<li class="divider"></li>
							<li><a href="sair.php">Sair</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="well borda">
				<div class="row"><!--divide pelas colunas da tela-->
				<div  class="col-xs-0 col-sm-0 col-md-1 col-lg-1"></div>
				<div  class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
					
					<img src="alunos/<?=$_SESSION['foto']?>" class="img-responsive img-rounded borda" width="285px">
					<form action="uploadfoto.php" method="post"  enctype="multipart/form-data" id="form">
						<div class="upload">
						<input type="file" name="foto" id="foto">
						</div>
					</form>
					<script type="text/javascript">
						document.getElementById("foto").onchange = function() {
    					document.getElementById("form").submit();
						}
					</script><br><br>	
					<font size="3px">
					<strong>Nome :</strong> <?=$_SESSION['nome']?><br>
					<strong>RM :</strong>  <?=$_SESSION['rm']?><br>
					<strong>Curso :</strong>  <?=$_SESSION['nomecurso']?><br>
					<?php
					if ($_SESSION['sexo']=="m") {
						$sexo="Masculino";
					}else{
						$sexo="Feminino";
					}
					?>
					<strong>Sexo :</strong>  <?=$sexo?><br>
					<strong>Email :</strong> <?=$_SESSION['email']?><br>
					<a href="alteraremail.php"><button class="btn btn-info" type="submit"><span class="glyphicon glyphicon-envelope"></span> Alterar Email</button></a>
					
					<a href="alterarsenha.php"><button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-edit"></span> Alterar Senha</button></a>
				</font>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
					<center><font size="4px"><strong>Alterar senha</strong></font></center>
					<form action="novoemail.php" method="post" accept-charset="utf-8">
						<center>
						<input type="hidden" name="rm" value="<?=$_SESSION['rm']?>">
						Novo email:<br> <input type="email" name="email1" required><br>
						Confirmação de email:<br> <input type="email" name="email2" required><br>
						<input type="checkbox" required>Quero alterar meu email<br>
						<button type="submit" name="enviar" class="btn btn-warning">Alterar</button></center>
					</form>

				</div>
			</div>
		</div>
	</div>
	<style type="text/css" media="screen">
		.borda{
			border:1px solid black;
			border-radius: 5px;
		}
	</style>
	<div class="container borda thumbnail">
		<div class="titulo "><center><strong><font size="5" color="black"><?=$_SESSION['nomecurso']?></font></strong></center></div><br>
		<div class="well" style="margin-top: -4px;">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<!--Começa repetição-->
			<?php
			$curso=$_SESSION['curso'];
			$comando="SELECT * FROM disciplina WHERE cod_curso = '$curso'";
			$enviar=mysqli_query($conn, $comando);
			$cursos=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
			foreach ($cursos as $curso) {
				$nomecurso=$curso['nome_disc'];
				$sigla=$curso['sigla'];
			?>
	<div class="dropdown">
	<button class="btn btn-default dropdown-toggle" type="button" id="aula" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 100%;">
	<?=$nomecurso?> (<?=$sigla?>)
	</button>
	<!--<a> TEMPORARIO-->
		<!--Pegar videos-->
		<div class="dropdown-menu" aria-labelledby="aula" style="width: 100%;">
		<?php
		$comando="SELECT * FROM video WHERE sigla = '$sigla' ORDER BY id_video DESC LIMIT 8";
		$enviar=mysqli_query($conn, $comando);
		$videos=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
		if ($videos) {
		foreach ($videos as $aula) {
			$id_aula=$aula['id_video'];
			$nomevideo=$aula['nome_video'];
			$data=$aula['data'];
			$link=$aula['link_video'];
		?>
		<!--Fim pegar videos-->
		<!--Exibir os videos-->

	
	<form action="video.php" method="post" style="float:left">
	<input type="hidden" name="sigla" value="<?=$sigla?>">
	<input type="hidden" name="id_video" value="<?=$id_aula?>">
	<button class="dropdown-item videos" type="submit" name="vervideo" ><img src="http://i1.ytimg.com/vi/<?=$link?>/mqdefault.jpg" width="140px"><br>
		<center>
	<?=$nomevideo?><br>
	<br>
</center>
	</button>
	</form>

	<?php
		}
	}else{
	?>
	<center>Nenhum video nessa matéria</center>
	<?php
	}
	?>
	<form action="procurar.php" method="post">
	<input type="hidden" name="sigla" value="<?=$sigla?>">
	<button class="dropdown-item" type="submit" style="width: 100%; text-align: left; border-style: none;background-color:transparent; color: gray; background-color: rgba(0,0,0,0.1);" name="escolhaaula"><center>Veja mais...</center>
	</button>
	</form>
	</div>
	</div>
	<br>
	<?php
	}
	?>
	
	<div class="titulo"><center><strong><font size="5" color="black">Ultimos vídeos</font></strong></center></div><br>
	<?php
	$curso=$_SESSION['curso'];
			$comando="SELECT * FROM video WHERE cod_curso = '$curso' ORDER BY data DESC";
			$enviar=mysqli_query($conn, $comando);
			$videos=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
			if ($videos) {
			foreach($videos as $video){
			$id_aula=$video['id_video'];
			$nomevideo=$video['nome_video'];
			$data=$video['data'];
			$link=$video['link_video'];
			$sigla=$video['sigla'];
		?>
	<form action="video.php" method="post" style="float:left">
	<input type="hidden" name="id_video" value="<?=$id_aula?>">
	<button class="dropdown-item videos" type="submit" name="vervideo"><br><img src="http://i1.ytimg.com/vi/<?=$link?>/mqdefault.jpg" width="140px"><br>
	<font size="2px"><?=mb_strimwidth($nomevideo,0,20,"...")?> <br><center>(<?=$sigla?>)</center></font><br>
	<br>
	</button>
	</form>
	<?php
		}
	?>
	
	<?php
	}

?>
	<center><br>
		<style type="text/css" media="screen">
		
		</style>
		<a href="procurar.php">
		<button class="vejamais">
			<font size="5px">Veja mais<br>
				<span class="glyphicon glyphicon-chevron-right">
				</span>
			</font>
		</button>
	</a>
	</center>
	</div>


							<!--Termina repetição (Repete várias vezes)-->
						</div>
					</div>
				</div>
			</div>
		</div>

	<br>
	<div class="footer">
		<br>
		<div class="row">
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/Etec_logo.png" class="img-responsive" width="80%"></center>
			</div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/cpslogo.png" class="img-responsive chao" width="90%"></center>
			</div>
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
		</div>
		<br><br><br><br>
	</div>
</body>
</html>