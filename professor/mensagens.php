<?php
session_start();
require_once("servidor.php");
if (isset($_SESSION['tipo'])) {
	if ($_SESSION['tipo']=="professor") {
	}else{
		$_SESSION['mensagem']="Você não tem permissão para ver esta página";
		header("location:../");
		exit;
	}
}
if (!empty($_SESSION['mensagem'])) {
	echo "<div class='btn-success'>";
	echo "<center>".$_SESSION['mensagem']."</center> 
	<a href='index.php'><span class='glyphicon glyphicon-remove' style='position:absolute;margin-left:85%; color:red;'></span></a>";
	unset($_SESSION['mensagem']);
	echo "</div>";
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<!--<span class="glyphicon glyphicon-headphones" aria-hidden="true">-->
		<link rel="shortcut icon" type="image/png" href="../favicon.ico"/>
		<title>EtecReplay - Mensagens</title>
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="estilo.css">
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.js"></script>
		
	</head>
	<style type="text/css" media="screen">
		.borda{
			border:2px solid black;
		}
	</style>
	<body style="overflow-x: hidden;">
		<nav class="navbar navbar-inverse">
			<div class="row">
				<div  class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-1">
					<a href="index.php"><img class="img-responsive" src="../	etecRlogo.png" style="margin: 14px 0px 0px 25px"></a>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-10">
					<ul class="nav navbar-nav">
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="index.php">Página Inicial</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="postagem.php">Novo vídeo</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="mensagens.php">Mensagens</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block" style="position: absolute; margin-left: 40%;"><a href="sair.php">Sair</a></li>
					</ul>
					<div class="dropdown visible-xs-block visible-xs-inline visible-xs-inline-block visible-sm-block visible-sm-inline visible-sm-inline-block" style="margin-top:10px; margin-left: 70%;" >
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width: 50px; height: 50px; background-color: black;">
						<span class="glyphicon glyphicon-align-justify"></span></button>
						<ul class="dropdown-menu">
							<li><a href="index.php">Pagina Inicial</a></li>
							<li><a href="postagem.php">Novo vídeo</a></li>
							<li><a href="mensagens.php">Mensagens</a></li>
							<li class="divider"></li>
							<li><a href="sair.php">Sair</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<div class="container">
			<div class="titulo borda"><center><strong><font size="5" color="black">Painel do Professor</font></strong></center></div>
			<div class="well borda" style="margin-top: -4px;">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-0 col-sm-0 col-md-1 col-lg-1">
							</div>
							<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
								
								<br><br>
								<style type="text/css" media="screen">
					input[type='file']
					{
					width: 30px;
					height: 30px;
					margin-left: 5px;
					opacity: 0;
					display: block;
					}
					.upload{
						position: absolute;
						margin-top: -30px;
						width: 30px;
						height: 30px;
						background-color: white;
						border-radius: 3px;
						background-image: url(../imagens/cameracerta.png);
					}
					.upload:hover{
						background-image: url(../imagens/cameracertahover.png);
					}
					</style>
					<img src="fotos/<?=$_SESSION['foto']?>" class="img-responsive img-rounded borda" width="185px">
					<form action="uploadfoto.php" method="post"  enctype="multipart/form-data" id="form">
						<div class="upload">
						<input type="file" name="foto" id="foto">
						</div>
					</form>
					<script type="text/javascript">
						document.getElementById("foto").onchange = function() {
    					document.getElementById("form").submit();
						}
					</script>
								<br>
								<font size="3px">
								<strong>Nome : </strong><?=$_SESSION['nome']?><br>
								<strong>Email : </strong> <?=$_SESSION['email']?><br>
								<strong>Aulas : </strong>
								<?php
								$cod_professor=$_SESSION['cod'];
								$email=$_SESSION['email'];
								$comando="SELECT * FROM professor WHERE email_prof = '$email'";
								$enviar=mysqli_query($conn, $comando);
								$resultado=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
								foreach ($resultado as $aula) {
								$sigla=$aula['sigla'];
								?>
								<?=$sigla?>
								<?php
								}
								?>
								</font>
								<br>
								
							</div>
							<div class="col-xs-12 col-sm-12 col-md-8 col-lg-7">
								<br>
								
								<center><font size="3px">Mensagens recebidas</font></center>
								<table class="table table-condensed borda">
									<thead>
										<tr>
											<th><center>Aluno</center></th>
											<th>Conteúdo</th>
											<th>Apagar</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$pesquisa = "SELECT * FROM mensagem WHERE cod_prof='$cod_professor'";
										$enviar=mysqli_query($conn, $pesquisa);
										$mensagens=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
											if ($mensagens) {
										foreach ($mensagens as $mensagem) {
											$id=$mensagem['id_mensagem'];
											$aluno=$mensagem['nome_aluno'];
											$email=$mensagem['email_aluno'];
											$cod_prof=$mensagem['cod_prof'];
											$mensagem=$mensagem['mensagem'];
										$pesquisa = "SELECT * FROM professor WHERE cod_prof = '$cod_prof'";
										$enviar=mysqli_query($conn, $pesquisa);
										$professor=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
										foreach ($professor as $professor) {
											$nome_professor=$professor['nome_prof'];
										}
										?>
										<tr>
											<td><a href="mailto:<?=$email?>"><?=$aluno?></a></td>
											<td><?=$mensagem?></td>
											<td>
												<form action="apagar.php" method="post">
													<input type="hidden" name="id_mensagem" value="<?=$id?>">
													<center>
													<button style="width: 100%;height: 35px; background-color: red;color:white;border-radius: 5px; border:2px solid black;" type="submit" name="apagar"><span class="glyphicon glyphicon-remove"></span></button></center>
												</form></td>
											</tr>
											<?php
											}
											}else{
											?>
											<tr>
												<td></td>
												<td>Nenhuma mensagem encontrada</td>
												<td></td>
											</tr>
											<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br><br><br><br><br><br><br><br><br><br>
		<div class="footer">
		<br>
		<div class="row">
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/Etec_logo.png" class="img-responsive" width="80%"></center>
			</div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/cpslogo.png" class="img-responsive chao" width="90%"></center>
			</div>
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
		</div>
		<br><br><br><br><br>
	</div>
</body>
	</html>