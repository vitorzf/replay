<?php
session_start();
require_once("servidor.php");
if (isset($_SESSION['tipo'])) {
	if ($_SESSION['tipo']=="professor") {
	}else{
		$_SESSION['mensagem']="Você não tem permissão para ver esta página";
		header("location:../");
		exit;
	}
}
if (!empty($_SESSION['mensagem'])) {
	echo "<div class='btn-success'>";
	echo "<center>".$_SESSION['mensagem']."</center> 
	<a href='index.php'><span class='glyphicon glyphicon-remove' style='position:absolute;margin-left:85%; color:red;'></span></a>";
	unset($_SESSION['mensagem']);
	echo "</div>";
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<!--<span class="glyphicon glyphicon-headphones" aria-hidden="true">-->
		<link rel="shortcut icon" type="image/png" href="../favicon.ico"/>
		<title>EtecReplay - Mensagens</title>
		<link rel="stylesheet" href="../css/bootstrap.css">
		<link rel="stylesheet" href="estilo.css">
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.js"></script>
		
	</head>
	<style type="text/css" media="screen">
		.borda{
			border:3px solid black;
		}
		.borda2{
		border-style: solid;
  	  	border-width: 3px 0px 0px 0px;
		}
	</style>
	<body style="overflow-x: hidden;">
		<nav class="navbar navbar-inverse">
			<div class="row">
				<div  class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-1">
					<a href="index.php"><img class="img-responsive" src="../	etecRlogo.png" style="margin: 14px 0px 0px 25px"></a>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-10">
					<ul class="nav navbar-nav">
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="index.php">Página Inicial</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="postagem.php">Novo vídeo</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="mensagens.php">Mensagens</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block" style="position: absolute; margin-left: 40%;"><a href="sair.php">Sair</a></li>
					</ul>
					<div class="dropdown visible-xs-block visible-xs-inline visible-xs-inline-block visible-sm-block visible-sm-inline visible-sm-inline-block" style="margin-top:10px; margin-left: 70%;" >
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width: 50px; height: 50px; background-color: black;">
						<span class="glyphicon glyphicon-align-justify"></span></button>
						<ul class="dropdown-menu">
							<li><a href="index.php">Pagina Inicial</a></li>
							<li><a href="postagem.php">Novo vídeo</a></li>
							<li><a href="mensagens.php">Mensagens</a></li>
							<li class="divider"></li>
							<li><a href="sair.php">Sair</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
		<div class="container-fluid">
			<div class="titulo borda"><center><strong><font size="5" color="black">Todos os seus vídeos</font></strong></center></div>
			<div class="well borda" style="margin-top: -4px;">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="row">
							<div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>
							<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
								<table class="table table-bordered borda">
									
										<?php
										$cod_prof=$_SESSION['cod'];
										$comando="SELECT * FROM video WHERE cod_prof = '$cod_prof' ORDER BY id_video DESC";
										$enviar=mysqli_query($conn, $comando);
										$recebe=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
										if ($recebe) {
											foreach ($recebe as $video) {
											$id_video=$video['id_video'];
											$titulo=$video['nome_video'];
											$descricao=$video['descricao'];
											$data=$video['data'];
											$sigla=$video['sigla'];
											$data=explode("-", $data);
											$tags=$video['tags'];
											if (empty($tags)) {
												$tags="Vídeo sem tags";
											}
											$link=$video['link_video'];
										?>
										<form action="mudarvideo.php" method="POST" accept-charset="utf-8">
										<tr>
									<input type="hidden" name="id_video" value="<?=$id_video?>">
									<input type="hidden" name="todos">
									<thead>
										<tr>
											<th><center>Nome vídeo</center></th>
											<td><center><input type="text" name="titulo" placeholder=" <?=$titulo?>"></center></td>
										</tr>
										<tr>
											<th><center>Descrição</center></th>
											<td><center><input type="text" name="descricao" placeholder=" <?=$descricao?>"></center></td>
										</tr>
										<tr>
											<th><center>Link</center></th>
											<td><center><input type="text" name="link" placeholder=" <?=$link?>"></center></td>
										</tr>
										<tr>
											<th><center>Tags</center></th>
											<td><center><input type="text" name="tags" placeholder=" <?=$tags?>"></center></td>
										</tr>
										<tr>
											<th><center>Data</center></th>
											<td><center><?=$data[2]."/".$data[1]."/".$data[0]?></center></td>
										</tr>
										<tr>
											<th><center>Aula</center></th>
											<td><center><?=$sigla?></center></td>
										</tr>
									</thead>
									<tbody>
										
				<tr>
					<th><center>Alterar | Apagar</center></th>
				<td>
				<button type="submit" name="alterar" class="btn- btn-primary" style="width: 16%;margin-left: 35%;float: left;"><span class="glyphicon glyphicon-send"></span></button>
									
									</form>
									<form action="apagarvideo.php" method="post">
								<input type="hidden" name="id_video" value="<?=$id_video?>">
								<button type="submit" name="apagar" class="btn- btn-danger" style="width: 16%;float: left;margin-left: 5px;"><span class="glyphicon glyphicon-remove"></span></button>
								</form>
									</td>
										</tr>		
										</tr>
										<tr class="borda2">
											<th><td></td></th>
										</tr>

											<?php
											}
											}else{
											?>
											<tr>
												<td></td>
												<td>Nenhum vídeo encontrado</td>
												<td></td>
											</tr>
											<?php
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-0 col-sm-0 col-md-2 col-lg-2"></div>
			</div>

		<div class="footer">
		<br>
		<div class="row">
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/Etec_logo.png" class="img-responsive" width="80%"></center>
			</div>
			<div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
				<center><img src="../imagens/cpslogo.png" class="img-responsive chao" width="90%"></center>
			</div>
			<div class="col-xs-1 col-sm-4 col-md-4 col-lg-4"></div>
		</div>
		<br><br><br><br><br>
	</div>
</body>
	</html>