<?php
session_start();
require_once("servidor.php");
if (isset($_SESSION['tipo'])) {
	if ($_SESSION['tipo']=="professor") {
	}else{
		$_SESSION['mensagem']="Você não tem permissão para ver esta página";
		header("location:../");
		exit;
	}
}

	if (isset($_GET['postar'])) {
		if (!empty($_GET['aula']) && !empty($_GET['nomevideo']) && !empty($_GET['link']) && !empty($_GET['descricao']) && !empty($_GET['data']) && !empty($_GET['cod_prof'])) {
			$aula=$_GET['aula'];
			$nomevideo=$_GET['nomevideo'];
			$link=$_GET['link'];
			$link = str_replace("https://www.youtube.com/watch?v=","", $link);
			$descricao=$_GET['descricao'];
			$data=$_GET['data'];
			$tags=$_GET['tags'];
			$cod_prof=$_GET['cod_prof'];

			//PROCURAR COD_CURSO COM A AULA ESCOLHIDA
			$comando="SELECT cod_curso FROM disciplina WHERE sigla = '$aula' LIMIT 1";
			$enviar=mysqli_query($conn, $comando);
			$resul=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
			if ($resul) {
				foreach ($resul as $codigo) {
				$cod_curso=$codigo['cod_curso'];
				}
			}else{
				echo "Sem resultado";
			}

			//

			$comando="INSERT INTO video(sigla, cod_prof, nome_video, link_video, descricao, tags, data, cod_curso)
			VALUES ('$aula', '$cod_prof', '$nomevideo', '$link', '$descricao', '$tags', '$data', '$cod_curso')";
			$enviar=mysqli_query($conn, $comando);
			if ($enviar) {
				$_SESSION['mensagem']="Video registrado com sucesso";
				header("location:index.php");
				exit;
			}else{
				$_SESSION['erro']="Erro ao registrar video";
				header("location:postagem.php");
				exit;
			}
		}else{
			$_SESSION['erro']="Algum dos campos ficou em branco";
			header("location:postagem.php");
			exit;
		}
	}else{
		header("location:../");
		exit;
	}
?>