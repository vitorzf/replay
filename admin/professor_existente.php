<?php
session_start();
include_once("servidor.php");
	if ($_SESSION['admin']=="Online"){
	}else{
		$_SESSION['mensagem']="Você não tem permissão para ver esta página";
		header("location:../");
		exit;
	}

	if (!empty($_SESSION['mensagem'])) {
	echo "<div class='btn-success'>";
	echo "<center>".$_SESSION['mensagem']."</center> 
	<a href='index.php'><span class='glyphicon glyphicon-remove' style='position:absolute;margin-left:85%; color:red;'></span></a>";
	unset($_SESSION['mensagem']);
	echo "</div>";
}

if (!empty($_SESSION['erro'])) {
	echo "<div class='btn-danger'>";
	echo "<center>".$_SESSION['erro']."</center> 
	<a href='index.php'><span class='glyphicon glyphicon-remove' style='position:absolute;margin-left:85%; color:red;'></span></a>";
	unset($_SESSION['erro']);
	echo "</div>";
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link rel="shortcut icon" type="image/png" href="favicon.ico"/>
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="estilo.css">
		
		<title>EtecReplay - Adicionar Professor</title>
	</head>
	<body style="overflow-x: hidden;">
		<nav class="navbar navbar-inverse">
			<div class="row">
				<div  class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-1">
					<a href="menu.php"><img class="img-responsive" src="login/etecRlogo.png" style="margin: 14px 0px 0px 25px"></a>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-10">
					<ul class="nav navbar-nav">
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="menu.php">Página Inicial</a></li>
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="sair.php">Sair</a></li>
					</ul>
					<div class="dropdown visible-xs-block visible-xs-inline visible-xs-inline-block visible-sm-block visible-sm-inline visible-sm-inline-block" style="margin-top:10px; margin-left: 70%;" >
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width: 50px; height: 50px; background-color: black;">
						<span class="glyphicon glyphicon-align-justify"></span></button>
						<ul class="dropdown-menu">
							<li><a href="menu.php">Pagina Inicial</a></li>
							<li><a href="sair.php">Sair</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>
<div class="tudo">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
				<div id="formulario">
					<br><br><br><br>
					<center>
						<div class="box">
						<br><br>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
							<a href="add_curso.php">
								<button class="menu"><span class="glyphicon glyphicon-arrow-down seta"></span>
									<font color="black">Novo Curso</font>
								</button>
							</a><br>
							<a href="add_disc.php">
								<button class="menu"><span class="glyphicon glyphicon-arrow-down seta"></span>
									<font color="black">Nova Disciplina</font>
								</button>
							</a><br>
							<a href="add_aluno.php">
								<button class="menu"><span class="glyphicon glyphicon-arrow-right seta"></span>
									<font color="black">Novo Aluno</font>
								</button>
							</a><br>
							<a href="add_turma.php">
								<button class="menu"><span class="glyphicon glyphicon-arrow-down seta"></span>
									<font color="black">Nova Turma</font>
								</button>
							</a><br>
							<a href="add_discturma.php">
								<button class="menu"><span class="glyphicon glyphicon-arrow-down seta"></span>
									<font color="black">Nova Disciplina na Turma</font>
								</button>
							</a><br>
							<a href="add_professor.php">
								<button class="menuescolhido"><span class="glyphicon glyphicon-arrow-down setaescolhida"></span>
									<font color="black">Novo Professor</font>
								</button>
							</a>
							
							<br><br>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 caixa ">
								<br><br>
				<?php
				if (isset($_POST['escolher'])) {
					$cod_prof=$_POST['cod_prof'];
					$comando="SELECT * FROM professor WHERE cod_prof = '$cod_prof'";
					$enviar=mysqli_query($conn, $comando);
					$recebe=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
					if ($recebe) {
						foreach ($recebe as $professor) {
							$nome=$professor['nome_prof'];
							$email=$professor['email_prof'];
							$senha=$professor['senha'];
							$data=$professor['dt_nasc_prof'];
							$telefone=$professor['tel_prof'];
							$foto=$professor['foto'];
						}
					}
				}
				?>
				<form action="criar_professor.php" method="post">
				<input type="hidden" name="profexist" value="sim">
				<input type="hidden" name="nome" value="<?=$nome?>">
				<input type="hidden" name="email" value="<?=$email?>">
				<input type="hidden" name="senha" value="<?=$senha?>">
				<input type="hidden" name="data" value="<?=$data?>">
				<input type="hidden" name="telefone" value="<?=$telefone?>">
				<input type="hidden" name="foto" value="<?=$foto?>">
	<p><center>Agora escolha a aula</center><br>

		Professor escolhido : <?=$nome?><br><br>
		<select name="sigla">
			<?php
			$comando="SELECT * FROM disciplina";
			$enviar=mysqli_query($conn, $comando);
			$recebe=mysqli_fetch_all($enviar, MYSQLI_ASSOC);
			if ($recebe) {
				foreach ($recebe as $curso) {
					$sigla=$curso['sigla'];
					$nome_disc=$curso['nome_disc'];
			?>
			<option value="<?=$sigla?>"><?=$sigla?> - <?=$nome_disc?></option>
			<?php
			
		}
	}
			?>
		</select>
	</p><br>
	<button type="submit" name="enviar" class="btn btn-info">Adicionar Professor</button>
	<br><br><br>
	</form>
					
							</div>
						</div>
					<br><br><br><br>
						</form>
					</div>
					</center>
				</div>
			</div>
		</div>
	</div>
	<footer style="margin-left:0px;
		height: 100%;
		background-color: #DCDCDC;
		border-style: solid;
  	  	border-width: 2px 0px 0px 0px;margin-top: 210px;">
  	  	<br><br>
		<img src="login/logo_CPS.png" width="20%" style="float: right; margin-right: 40px;">
		<img src="login/fundo.png" width="20%">
		</footer>
		</body>
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.js"></script>
	</html>

