<?php
session_start();
require_once("servidor.php");
if (!empty($_SESSION['mensagem'])) {
	echo "<div class='btn-success'>";
	echo "<center>".$_SESSION['mensagem']."</center> 
	<a href='index.php'><span class='glyphicon glyphicon-remove' style='position:absolute;margin-left:85%; color:red;'></span></a>";
	unset($_SESSION['mensagem']);
	echo "</div>";
}
if (!empty($_SESSION['erro'])) {
	echo "<div class='btn-danger'>";
	echo "<center>".$_SESSION['erro']."</center> 
	<a href='index.php'><span class='glyphicon glyphicon-remove' style='position:absolute;margin-left:85%; color:red;'></span></a>";
	unset($_SESSION['erro']);
	echo "</div>";
}
session_destroy();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
		<link rel="shortcut icon" type="image/png" href="favicon.ico"/>
		<link rel="stylesheet" href="css/bootstrap.css">
		<title>EtecReplay - Login</title>
	</head>
	<body style="overflow-x: hidden;">
		<nav class="navbar navbar-inverse">
			<div class="row">
				<div  class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-1">
					<a href="index.php"><img class="img-responsive" src="login/etecRlogo.png" style="margin: 14px 0px 0px 25px"></a>
				</div>
				<div class="col-xs-8 col-sm-8 col-md-8 col-lg-10">
					<ul class="nav navbar-nav">
						<li class="visible-lg-block visible-lg-inline visible-lg-inline-block visible-md-block visible-md-inline visible--inline-block"><a href="../../index.php">Página Inicial</a></li>
						
					</ul>
					<div class="dropdown visible-xs-block visible-xs-inline visible-xs-inline-block visible-sm-block visible-sm-inline visible-sm-inline-block" style="margin-top:10px; margin-left: 70%;" >
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="width: 50px; height: 50px; background-color: black;">
						<span class="glyphicon glyphicon-align-justify"></span></button>
						<ul class="dropdown-menu">
							<li><a href="../index.php">Pagina Inicial</a></li>
							
						</ul>
					</div>
				</div>
			</div>
		</nav>
<div class="tudo">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<style type="text/css" media="screen">
					.box{
						border: 3px solid black;
						max-width: 400px;
						border-radius: 10px;
						background-color: #CDCDCD;
					}
				</style>
				<div id="formulario">
					<br><br><br><br>
					<center>
						<div class="box">
						<br><br>
						<img src="etecRlogo.png" width="150px"><sub><font size="4px">Admin</font></sub><br><br><br>
						<form action="checagem.php" method="post" accept-charset="utf-8">
						<strong>LOGIN</strong><br>	<input type="text" name="login"><br>
						<strong>SENHA</strong><br>	<input type="password" name="senha"><br><br>
					<button class="btn btn-default" type="submit" name="enviar">Entrar</button>
					<br><br><br><br>
						</form>
					</div>
					</center>
				</div>
			</div>
		</div>
	</div>
	<footer style="margin-left:0px;
		height: 100%;
		background-color: #DCDCDC;
		border-style: solid;
  	  	border-width: 2px 0px 0px 0px;margin-top: 210px;">
  	  	<br><br>
		<img src="logo_CPS.png" width="20%" style="float: right; margin-right: 40px;">
		<img src="fundo.png" width="20%">
		</footer>
		</body>
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.js"></script>
	</html>